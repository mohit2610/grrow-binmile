import React from "react";
import Header from "./commonComponent.js/header/Header";
import HomeScreenCardViews from "./components/homeScreenComponent/HomeScreen";
import KeepLearnKeepGrow from "./components/articlesComponent/Articles";
import Investor from "./components/investorsComponent/Investors";
import OurProduct from "./components/ourProduct/OurProduct";
import ErrorHandler from "./commonComponent.js/errorHandler/ErrorHandler";
import "./styles.css";
import Footer from "./commonComponent.js/footer";
import TrustedComponent from "./components/trustedComponent/TrustedComponent";
import Press from "./components/pressComponent/Press"
import SupportComponent from "./components/supportComponent/SupportComponent";
import InvestComponent from "./components/investComponent/InvestComponent";

const Main = () => {
    return (
        <div>
            <ErrorHandler><Header /></ErrorHandler>
            <ErrorHandler><HomeScreenCardViews /></ErrorHandler>
            <ErrorHandler><OurProduct /></ErrorHandler>
            <ErrorHandler><TrustedComponent /></ErrorHandler>
            <ErrorHandler> <KeepLearnKeepGrow /></ErrorHandler>
            <Press />
            <ErrorHandler><Investor /></ErrorHandler>
            <InvestComponent />
            <SupportComponent />
            <ErrorHandler><Footer /></ErrorHandler>

        </div>
    );
}
export default Main; 