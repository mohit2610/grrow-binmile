import { createSlice } from '@reduxjs/toolkit';
const initialState = {
    value: {
        email: '',
        otp: '',
        password: ''
    },
}

export const counterSlice = createSlice({
    name: 'counter',
    initialState,
    reducers: {
        handleChange: (state, action) => {
            const { type, value } = action.payload;
            if (type === 'email') {
                state.value.email = value;
            }
            else if (type === 'otp') {
                state.value.otp = value;
            }
            else {
                state.value.password = value;
            }
        },
    },
})
export const { handleChange } = counterSlice.actions

export default counterSlice.reducer