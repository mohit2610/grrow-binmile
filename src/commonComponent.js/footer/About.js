import { Avatar, Container, Grid } from '@mui/material';
import React from 'react';
import GrrowLogoLight from "../../assets/images/footerImages/logo-light-groww.1815ad63.svg";
import AppStore from "../../assets/images/footerImages/app-store-logo.060773ea.svg";
import GoogleStore from "../../assets/images/footerImages/google-play-badge.0547a72f.svg";
import "./about.css"
import Divider from '@mui/material/Divider';

export default function About() {
    return (
        <div>
            <div className='about-div-main'>
                <Container>
                    <Grid container
                        direction="row"
                        columns={24}>
                        <Grid item xs={6} md={6} className="grow-about-image">
                            <div className='about-grid-one '>
                                <Avatar variant={"rounded"} alt="The image" src={GrrowLogoLight} style={{
                                    width: 150,
                                    height: 40,
                                    "marginBottom": "35px",
                                    "marginTop": "12px"
                                }} />
                                <div>
                                    <p className='colour-text align-start'>No.11, 2nd floor, 80 FT Road</p>
                                    <p className='colour-text align-start'>6th Block, S.T Bed, Koramangala</p>
                                    <p className='colour-text align-start'>Bengaluru – 560036</p>
                                    <p className='colour-text align-start'>Contact Us</p>
                                </div>
                            </div>
                        </Grid>
                        <Grid item xs={6} md={6}>
                            <p className='colour-text align-start font-size'>Products</p>
                            <p className='colour-text align-start'>Stocks</p>
                            <p className='colour-text align-start'>Future & Options</p>
                            <p className='colour-text align-start'>Mutual Funds</p>
                            <p className='colour-text align-start'>Fixed Deposit</p>
                            <p className='colour-text align-start'>US Stocks</p>
                        </Grid>
                        <Grid item xs={6} md={6}>
                            <p className='colour-text align-start font-size'>Groww</p>
                            <p className='colour-text align-start'>About Us</p>
                            <p className='colour-text align-start'>Pricing</p>
                            <p className='colour-text align-start'>Blog</p>
                            <p className='colour-text align-start'>Media & Press</p>
                            <p className='colour-text align-start'>Careers</p>
                            <p className='colour-text align-start'>Help and Support</p>
                        </Grid>
                        <Grid item xs={6} md={6}><p className='colour-text align-start font-size'>QUICK LINKS</p>
                            <p className='colour-text align-start'>AMC Mutual Funds</p>
                            <p className='colour-text align-start'>Calculators</p>
                            <p className='colour-text align-start'>Glossary</p>
                            <p className='colour-text align-start'>Open Demat Account</p>
                            <p className='colour-text align-start'>Groww Digest</p>
                            <p className='colour-text align-start'>Groww Academy</p>
                            <p className='colour-text align-start'>Sitemap</p></Grid>
                    </Grid>
                </Container>
                <Divider className='divider-white' />
            </div>
            <div className='about-grid-two'>
                <Grid container
                    direction="row"
                    columns={24}>
                    <Grid item xs={12} md={12} >
                        <p className='colour-text align-start'>ⓒ 2016-2020 Groww. All rights reserved, Built with <span className='red-colour'>♥</span> in India</p>
                    </Grid>
                        <Grid item xs={12} md={12}>
                            <div className='image-horizontal'>
                            <Avatar variant={"rounded"} alt="The image" src={AppStore} style={{
                                width: 120,
                                height: 40,
                                "marginBottom": "35px",
                                "marginTop": "12px",
                                "marginRight":"15px"
                            }} />
                             <Avatar variant={"rounded"} alt="The image" src={GoogleStore} style={{
                                width: 120,
                                height: 35,
                                "marginBottom": "35px",
                                "marginTop": "12px"
                            }} />
                            </div>
                    </Grid>
                </Grid>
            </div>
        </div>
    )
}
