import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import InputBase from "@material-ui/core/InputBase";
import SearchIcon from "@material-ui/icons/Search";
import React, { useContext, useState } from "react";
import "../../styles.css";
import { Avatar, Button } from "@material-ui/core";
import GrrowLogo from "../../assets/images/headerImages/grrow_logo.svg";
import { useStyles } from "./HeaderStyle";
import LoginForm from "../../components/loginComponent/LoginForm";
import { LocalizationContext, LANGUAGE_ENGLISH, LANGUAGE_FRENCH } from "../../localization/Localization";


export default function Header() {
    const en = "eng";
    const fr = 'frn';
    const { setAppLanguage } = useContext(LocalizationContext);
    const langChange = (lang) => {
        if (lang === en) {
            setAppLanguage(LANGUAGE_ENGLISH);
        } else {
            setAppLanguage(LANGUAGE_FRENCH);
        }
    }
    const [modal, setModal] = useState(false)
    const { translations } = useContext(LocalizationContext);
    const openModal = () => {
        setModal(true);
    }
    const closeModal = () => {
        setModal(false);
    }
    const classes = useStyles();
    return (
        <div className="App">
            <div className={classes.grow}>
                <AppBar position="static">
                    <Toolbar>
                        <Avatar variant={"rounded"} alt="The image" src={GrrowLogo} style={{
                            width: 100,
                            height: 27,
                        }} />
                        <div className={classes.search}>
                            <div className={classes.searchIcon}>
                                <SearchIcon />
                            </div>
                            <InputBase
                                placeholder="Search…"
                                classes={{
                                    root: classes.inputRoot,
                                    input: classes.inputInput
                                }}
                                inputProps={{ "aria-label": "search" }}
                            />
                        </div>
                        <div className={classes.grow} />
                        <Button className="button" onClick={openModal} variant="outlined">{translations.LoginRegister}</Button>
                        <Button className="button-lang" onClick={() => langChange(en)} variant="outlined">English</Button>
                        <Button className="button-lang" onClick={() => langChange(fr)} variant="outlined">French</Button>
                    </Toolbar>
                </AppBar>
                {modal ? <LoginForm closeModal={closeModal} modal={modal} /> : ""}
            </div>
        </div>
    );
}
