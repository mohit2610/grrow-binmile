import React from 'react';
import Typography from '@mui/material/Typography';
import Grid from "@mui/material/Grid";

export default function InvestComponent() {
    
    return (
        <div>
            <Grid alignItems="flex-start" container spacing={1} columns={24}>
                <Grid container direction="column" item xs={8} spacing={2} >
                    <Typography variant="h4" component="div">
                        Invest anyWhere,<br /> anytime.
                    </Typography>
                    <Typography variant="subtitle2" component="div" >
                        {`don't`} worry about which device<br />
                         in every one of them.
                    </Typography>
                    

                </Grid>
            </Grid>
        </div >
    )
}
