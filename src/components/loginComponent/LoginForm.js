import React, { useEffect, useState } from 'react';
// import { auth } from '../../firebase/Firebase'
import Modal from '@mui/material/Modal';
import Button from '@mui/material/Button';
import GoogleIcon from '@mui/icons-material/Google';
import Grid from "@mui/material/Grid";
import TextField from '@mui/material/TextField';
import './LoginForm.css';
import swal from 'sweetalert';
import { Box, Container } from '@mui/material';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import { withStyles } from "@material-ui/core/styles";
import LinearProgress from "@material-ui/core/LinearProgress";
import { useDispatch, useSelector } from 'react-redux';
import { handleChange } from '../../Redux-Tolkit/CreateSlice'
const textArray = ['ETFs', 'Gold', 'US Stocks', 'Fixed Deposits', 'Stocks', 'Direct Mutual Funds'];

function LoginForm({ closeModal }) {
  const [seconds, setSeconds] = useState(0);
  const [showOtp, setShowOtp] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const dispatch = useDispatch()
  const Email = useSelector((state) => state.Change.value.email);
  const OTP = useSelector((state) => state.Change.value.otp);
  const Password = useSelector((state) => state.Change.value.password)


  useEffect(() => {
    const interval = setInterval(() => {
      setSeconds(seconds => seconds + 1);
    }, 2000);
    return () => clearInterval(interval);
  }, []);

  const SlowLinearProgress = withStyles({
    bar: {
      animationDuration: "4s",
    }
  })(LinearProgress);

  let textThatChanges = textArray[seconds % textArray.length];

  const onContinue = () => {
    const emailRegax = /\S+@\S+\.\S+/
    if (Email !== '' && Email.match(emailRegax)) {
      // onSignInSubmit()
      setShowOtp(true);
    }
    else {
      swal("Please Enter a Valid Email");

    }
  }
  const onSubmit = () => {
    setShowOtp(false);
    setShowPassword(true);
  }

  // const onSignInSubmit = () => {
  //   auth.createUserWithEmailAndPassword(Email, "1234wer")
  //     .then((userCredential) => {
  //       // send verification mail.
  //       userCredential.user.sendEmailVerification();
  //       auth.signOut();
  //       alert("Email sent");
  //     })
  //     .catch(alert);
  // }
  return (
    <React.Fragment>
      <Modal
        className="login-form-modal"
        open={true}
        aria-labelledby="child-modal-title"
        aria-describedby="child-modal-description"
      >
        <Container>
          <Grid container className='loginForm-mainGrid'
            direction="row"
            
            columns={16}>
            <Grid item xs={8} md={8}>
              <div className="loginForm-imageDiv">
                Simple, Free <br /> Investing.
                <div className="loginform-imagediv-downside">
                  <SlowLinearProgress />
                  {textThatChanges}</div>
              </div>
            </Grid>
            <Grid item xs={8} md={8}>
              <div className="login-formDiv">
                <Box className='login-closeButton' >
                  <IconButton onClick={closeModal}>
                    <CloseIcon />
                  </IconButton>
                </Box>
                <form>
                  <Grid item xs={24} md={24}>
                    <h1 className='continueWithGoogle'>Welcome to Groww</h1>
                  </Grid>
                  {showOtp || showPassword ? "" : <div >
                    <Grid item xs={24} md={24}>
                      <Button variant="outlined" startIcon={<GoogleIcon />} className='googlebutton'>Continue with Google</Button>
                    </Grid>
                  </div>
                  }
                  {showOtp || showPassword ? "" : <div className='loginForm-or-line'>Or</div>}
                  <div>
                    <Grid item xs={24} md={24}>
                      <TextField id="standard-basic"
                        label="Your Email Address"
                        variant="standard"
                        className='loginForm-email-field'
                        value={Email}
                        onChange={e => dispatch(handleChange({ value: e.target.value, type: 'email' }))}
                      />
                    </Grid>
                  </div>
                  {showOtp ? <div>
                    <Grid item xs={24} md={24}>
                      <TextField id="standard-basic"
                        label="OTP Code"
                        type="number"
                        variant="standard"
                        className='loginForm-email-field'
                        value={OTP}
                        onChange={e => dispatch(handleChange({ value: e.target.value, type: 'otp' }))}
                      />
                    </Grid>
                  </div> : showPassword ? <div>
                    <Grid item xs={24} md={24}>
                      <TextField
                        label="Set Password"
                        variant="standard"
                        className='loginForm-email-field'
                        value={Password}
                        onChange={e => dispatch(handleChange({ value: e.target.value, type: 'password' }))}
                      />
                    </Grid>
                  </div> : ""}
                  <div className='login-continue-button'>
                    {!showOtp ?
                      <Grid item xs={24} md={24}>
                        <Button onClick={onContinue} className='login-form-continue-button' >Continue</Button>
                      </Grid>
                      : <Grid item xs={24} md={24}><Button onClick={onSubmit} className='login-form-continue-button' >Submit</Button></Grid>}
                  </div>
                  <div>
                    <Grid item xs={24} md={24}>
                      <p className='login-form-privacypolicy-text'>By proceeding ,I agree to <span className='bold-policy-login'>T&C</span> and <span className='bold-policy-login'>Privacy Policy</span></p>
                    </Grid>
                  </div>
                </form>
              </div>
            </Grid>
          </Grid>
        </Container>
      </Modal>
    </React.Fragment>
  );
}
export default LoginForm;

