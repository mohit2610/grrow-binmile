import * as React from 'react';
import "./OurProduct.css";
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import ShowChartIcon from '@mui/icons-material/ShowChart';
import AccountBalanceIcon from '@mui/icons-material/AccountBalance';
import AttachMoneyIcon from '@mui/icons-material/AttachMoney';
import ReorderIcon from '@mui/icons-material/Reorder';
import Grid from "@mui/material/Grid";
import ourProductsImage from "../../assets/images/ourProductImages/ourProductStocks.png";
import mutualFund from "../../assets/images/ourProductImages/mutualFund.png";
import usStocks from "../../assets/images/ourProductImages/usStocks.png";
import FDsImage from "../../assets/images/ourProductImages/FDsImage.png";
import OurProductButton from './OurProductButton';


export default function OurProduct() {
    const [value, setValue] = React.useState(0);
    
    const handleChange = (event, newValue) => {
        setValue(newValue);
    };
    return (
        <div className='our-product-main-div'>
            <div className='box'>
                <div className='top-2-heading'>
                    <Typography variant="body2" gutterBottom className='our-product-button'>
                        OUR PRODUCTS
                    </Typography>
                    <Typography variant="h4" gutterBottom component="div" className='our-product-h4'>
                        Your money. Your choice.
                    </Typography>
                </div>
                <div>
                    <Box sx={{
                        width: '100%',
                        float: "left",
                        bgcolor: 'background.paper'
                    }}>
                        <Tabs value={value} onChange={handleChange} >
                            <Tab className='item1' icon={<ShowChartIcon />} iconPosition="start" label="Stocks" />
                            <Tab className='item2' icon={<ReorderIcon />} iconPosition="start" label="Mutual Funds" />
                            <Tab className='item3' icon={<AttachMoneyIcon />} iconPosition="start" label="US Stocks" />
                            <Tab className='item4' icon={<AccountBalanceIcon />} iconPosition="start" label="FDs" />
                        </Tabs>
                        <TabPanel value={value} index={0}>
                            <div >
                                <div className="keeplearn-grid">
                                    <Grid alignItems="flex-start" container spacing={1}>
                                        <Grid container direction="column" item xs={4} spacing={1}>
                                            <Grid item xs={12} >
                                                <Box
                                                    component="img"
                                                    sx={{
                                                        boxShadow: 1,
                                                        width: '30rem',
                                                        height: '22rem',
                                                        bgcolor: (theme) => (theme.palette.mode === 'dark' ? '#101010' : '#fff'),
                                                        color: (theme) =>
                                                            theme.palette.mode === 'dark' ? 'grey.300' : 'grey.800',
                                                        borderRadius: 5,
                                                        textAlign: 'center',
                                                        fontSize: '0.875rem',
                                                        fontWeight: '700',
                                                        marginLeft: '100px',
                                                    }}
                                                    alt="The house from the offer."
                                                    src={ourProductsImage}
                                                />
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </div>
                                <div className='side-part-stocks' >
                                    <div className='zeroFee-product-upper'>
                                        <Typography variant="h1" component="div" className='zerofee_product'>
                                            Zero <span className='account_charges-span'>account Chanrges</span>
                                        </Typography>
                                    </div>
                                    <div >
                                        <Typography className='accountChareges_subheading' >you dont have to pay a single rupee for opening a stocks
                                            account or account maintenance.</Typography>
                                    </div>
                                    <div className='product-button'>
                                        <OurProductButton />
                                    </div>
                                </div>
                            </div>
                        </TabPanel>
                        <TabPanel value={value} index={1}>
                            <div >
                                <div className="keeplearn-grid">
                                    <Grid alignItems="flex-start" container spacing={1}>
                                        <Grid container direction="column" item xs={4} spacing={1}>
                                            <Grid item xs={12} >
                                                <Box
                                                    component="img"
                                                    sx={{
                                                        boxShadow: 1,
                                                        width: '30rem',
                                                        height: '22rem',
                                                        bgcolor: (theme) => (theme.palette.mode === 'dark' ? '#101010' : '#fff'),
                                                        color: (theme) =>
                                                            theme.palette.mode === 'dark' ? 'grey.300' : 'grey.800',
                                                        borderRadius: 5,
                                                        textAlign: 'center',
                                                        fontSize: '0.875rem',
                                                        fontWeight: '700',
                                                        marginLeft: '100px',
                                                    }}
                                                    alt="The house from the offer."
                                                    src={mutualFund}
                                                />
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </div>
                                <div className='side-part-stocks' >
                                    <div className='zeroFee-product-upper'>
                                        <Typography variant="h1" component="div" className='zerofee_product'>
                                            0% <span className='account_charges-span'>commision</span>
                                        </Typography>
                                    </div>
                                    <div >
                                        <Typography className='accountChareges_subheading'>select from 5000+ direct mutual funds and get higher return
                                            than regular funds.</Typography>
                                    </div>
                                    <div className='product-button'>
                                        <OurProductButton />
                                    </div>
                                </div>
                            </div>
                        </TabPanel>
                        <TabPanel value={value} index={2}>
                            <div >
                                <div className="keeplearn-grid">
                                    <Grid alignItems="flex-start" container spacing={1}>
                                        <Grid container direction="column" item xs={4} spacing={1}>
                                            <Grid item xs={12} >
                                                <Box
                                                    component="img"
                                                    sx={{
                                                        boxShadow: 1,
                                                        width: '30rem',
                                                        height: '22rem',
                                                        bgcolor: (theme) => (theme.palette.mode === 'dark' ? '#101010' : '#fff'),
                                                        color: (theme) =>
                                                            theme.palette.mode === 'dark' ? 'grey.300' : 'grey.800',
                                                        borderRadius: 5,
                                                        textAlign: 'center',
                                                        fontSize: '0.875rem',
                                                        fontWeight: '700',
                                                        marginLeft: '100px',
                                                    }}
                                                    alt="The house from the offer."
                                                    src={usStocks}
                                                />
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </div>
                                <div className='side-part-stocks' >
                                    <div className='zeroFee-product-upper'>
                                        <Typography variant="h1" component="div" className='zerofee_product'>
                                            Free <span className='account_charges-span'>accout opening</span>
                                        </Typography>
                                    </div>
                                    <div>
                                        <Typography className='accountChareges_subheading'>Invest in Apple,Google,Netflix and many
                                            more US companies  that you     love    without any brokerage fee.</Typography>
                                    </div>
                                    <div className='product-button'>
                                        <OurProductButton />
                                    </div>
                                </div>
                            </div>
                        </TabPanel>
                        <TabPanel value={value} index={3}>
                            <div >
                                <div className="keeplearn-grid">
                                    <Grid alignItems="flex-start" container spacing={1}>
                                        <Grid container direction="column" item xs={4} spacing={1}>
                                            <Grid item xs={12} >
                                                <Box
                                                    component="img"
                                                    sx={{
                                                        boxShadow: 1,
                                                        width: '30rem',
                                                        height: '22rem',
                                                        bgcolor: (theme) => (theme.palette.mode === 'dark' ? '#101010' : '#fff'),
                                                        color: (theme) =>
                                                            theme.palette.mode === 'dark' ? 'grey.300' : 'grey.800',
                                                        borderRadius: 5,
                                                        textAlign: 'center',
                                                        fontSize: '0.875rem',
                                                        fontWeight: '700',
                                                        marginLeft: '100px',
                                                    }}
                                                    alt="The house from the offer."
                                                    src={FDsImage}
                                                />
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </div>
                                <div className='side-part-stocks' >
                                    <div className='zeroFee-product-upper'>
                                        <Typography variant="h1" component="div" className='zerofee-product'>
                                            6.7% <span className='account_charges-span'>interest rate</span>
                                        </Typography>
                                    </div>
                                    <div>
                                        <Typography className='accountChareges_subheading'> Open fixed deposite in any bank with higher
                                            interest rates without opening a bank account. </Typography>
                                    </div>
                                    <div className='product-button'>
                                        <OurProductButton />
                                    </div>
                                </div>
                            </div>
                        </TabPanel>
                    </Box>
                </div>
            </div>

        </div>
    );
}

function TabPanel(props) {
    const { children, value, index } = props;
    return (<div>{
        value === index && (
            <h1>{children}</h1>
        )
    }

    </div>)
}
