import React, { useState } from 'react'
import Button from '@mui/material/Button';
import LoginForm from "../../components/loginComponent/LoginForm";

export default function OurProductButton() {
  const [modal, setModal] = useState(false);

  const openModal = () => {
    setModal(true);
  }
  const closeModal = () => {
    setModal(false);
  }

  return (
    <div>
      <Button onClick={openModal} className="button" variant="outlined">Craete Account For Free</Button>
      {modal ? <LoginForm closeModal={closeModal} modal={modal} /> : ""}
    </div>
  )

}
