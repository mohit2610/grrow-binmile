import React from 'react';
import Grid from '@mui/material/Grid';
import { Container } from '@mui/material'
import Divider from '@mui/material/Divider';
import "./Press.css";
import Typography from '@mui/material/Typography';
import { Avatar } from '@mui/material';
import BusinessLine from "../../assets/images/pressComponentsImage/BusinessLine.svg";
import PressBusinessLine from "../../assets/images/pressComponentsImage/PressBusinessLine.svg";
import PressLiveMint from "../../assets/images/pressComponentsImage/PressLiveMint.svg";
import PressYourStory from "../../assets/images/pressComponentsImage/PressYourStory.svg";

export default function Press() {

    return (
        <div>
            <Divider className='divider'>
                <Typography variant="h4" component="div">
                    In The Press
                </Typography>
            </Divider>
            <Container>
                <Grid container
                    direction="column"
                    columns={24}>
                    <div className='pressImages'>
                        <Grid item xs={6} md={6}>
                            <Avatar variant={"rounded"} alt="The image" src={BusinessLine} style={{
                                width: 150,
                                height: 40,
                                "marginBottom": "35px",
                                "marginTop": "12px"
                            }} />
                        </Grid>
                        <Grid item xs={6} md={6}>
                            <Avatar variant={"rounded"} alt="The image" src={PressBusinessLine} style={{
                                width: 150,
                                height: 40,
                                "marginBottom": "35px",
                                "marginTop": "12px"
                            }} />
                        </Grid>
                        <Grid item xs={6} md={6}>
                            <Avatar variant={"rounded"} alt="The image" src={PressLiveMint} style={{
                                width: 150,
                                height: 40,
                                "marginBottom": "35px",
                                "marginTop": "12px"
                            }} />
                        </Grid>
                        <Grid item xs={6} md={6}>
                            <Avatar variant={"rounded"} alt="The image" src={PressYourStory} style={{
                                width: 150,
                                height: 40,
                                "marginBottom": "35px",
                                "marginTop": "12px"
                            }} />
                        </Grid>
                    </div>
                </Grid>

            </Container>
        </div>
    );
}
