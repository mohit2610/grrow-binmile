import React from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import "./SupportComponent.css";
import Grid from "@mui/material/Grid";
import { Avatar } from '@mui/material';
import SupportComponentImage from '../../assets/images/supportComponent/SupportComponentImage.png'



export default function SupportComponent() {
    return (
        <Box
            sx={{
                width: 1000,
                height: 300,
                backgroundColor: '#34eb98',
                margin: '20px auto',
                marginTop: '40px',
                marginBottom: '40px',
                borderRadius: '20px',

            }}
        >
            <div>
                <Grid alignItems="flex-start" container spacing={1} columns={24}>
                    <Grid container direction="column" item xs={8} spacing={2} >
                        <Grid item xs={6} >
                            <Typography variant="h4" component="div" className='subtitles1'>
                                We were  With you, at every step
                            </Typography>
                            <Typography variant="subtitle2" component="div" className='subtitles2'>
                                For any query you have , find the answer quickly on our Help & support.<br/>
                                Need a little more help ? we are happy to talk via call or chat.
                            </Typography>
                            <Button className="buttonSupport" variant="outlined">Get in Touch</Button>
                        </Grid>
                    </Grid>
                    <Grid direction="column" item xs={8}  >
                        <Avatar variant={"rounded"} alt="The image" src={SupportComponentImage} style={{
                            width: 200,
                            height: 272,
                            marginTop:"20px",
                        }} />
                    </Grid>
                </Grid>

            </div>
        </Box >


    )
}
