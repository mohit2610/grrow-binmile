import { Box, Grid } from '@material-ui/core';
import React from 'react';
import Particles from "react-tsparticles";
import './trustedComponent.css'
import TrustedImage from '../../assets/images/trustedImages/shield-groww.webp'

export default function TrustedComponent() {
    const particlesInit = (main) => {
        console.log(main);
    };

    const particlesLoaded = (container) => {
        console.log(container);
    };
    return (
        <div >
            <div className='particles-div-color'>
                <Particles
                    init={particlesInit}
                    loaded={particlesLoaded}
                    options={{
                        fpsLimit: 400,
                        interactivity: {
                            modes: {
                                bubble: {
                                    distance: 20,
                                    duration: 2,
                                    opacity: 0.1,
                                    size: 1,
                                }
                            },
                        },
                        particles: {
                            links: {
                                color: "#ffffff",
                                distance: 10,
                                enable: true,
                                // opacity: 20,
                                width: 1,
                            },
                            move: {
                                direction: "none",
                                enable: true,
                                outMode: "bounce",
                                random: false,
                                speed: 1,
                                straight: false,
                            },
                            shape: {
                                type: "circle",
                            },
                            size: {
                                random: true,
                                value: 8,
                            },
                        }
                    }}
                />
                <Grid container>
                    <Grid item  >
                        <div style={{ "display": "flex" }}>
                            <div className='heading-class-main'>
                                <p className='trusted-heading align-start bold-class'>Trusted by
                                </p>
                                <p className='trusted-heading align-start bold-class'>20 Million+ users
                                </p>
                                <p className='trusted-heading align-start font-class'>Our cutting-edge technology ensure that all your information remains
                                </p>
                                <p className='trusted-heading font-class align-start'>fully encrypted and secure .
                                </p>
                            </div>
                            <div>
                                <Box
                                    component="img"
                                    sx={{
                                        boxShadow: 1,
                                        width: '45rem',
                                        height: '40rem',
                                        bgcolor: (theme) => (theme.palette.mode === 'dark' ? '#101010' : '#fff'),
                                        color: (theme) =>
                                            theme.palette.mode === 'dark' ? 'grey.300' : 'grey.800',
                                        borderRadius: 5,
                                        textAlign: 'center',
                                        fontSize: '0.875rem',
                                        fontWeight: '700',
                                    }}
                                    alt="The house from the offer."
                                    src={TrustedImage}
                                />
                            </div>
                        </div>
                    </Grid>
                </Grid>
            </div>
        </div>
    );
}