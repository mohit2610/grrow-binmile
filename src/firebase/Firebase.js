import firebase from "firebase/compat/app";
import "firebase/compat/auth";

const app = firebase.initializeApp({
    apiKey: "AIzaSyAQZ7EtsFEPH36Mt5ONZO2oTl80FTen3vU",
    authDomain: "grrow-a1674.firebaseapp.com",
    projectId: "grrow-a1674",
    storageBucket: "grrow-a1674.appspot.com",
    messagingSenderId: "144591186387",
    appId: "1:144591186387:web:aa6b7f6cf035c963c52726",
    measurementId: "G-BTJ0H6EYGP"
})
export const auth = app.auth();
export default app;