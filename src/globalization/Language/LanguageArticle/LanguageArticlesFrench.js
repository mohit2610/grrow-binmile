export default {
   KeepLearningKeepGrowing : "Continue d'apprendre. Continuez à grandir",
   SelectStocks :"Comment sélectionner/choisir des actions pour l'intrajournalier",
   GuideMutualFunds :"Guide du débutant sur les fonds communs de placement",
   DiversifyVolatility:"Comment se diversifier en période de volatilité des marchés",
}