export default {
    FDInvest: "Investir dans",
    FDInvestDescription: "Approuvé par des millions d'Indiens. Commencez à investir aujourd'hui",
    GetStarted: "Commencer",
    Stocks: "Actions",
    FuturesOptions: "Contrats à terme et options",
    USStocks: "Actions américaines",
    IPO:"introduction en bourse",
    FixedDeposits:"Dépôts fixes",
    MutualFunds: "Fonds communs de placement",

}