export default {
    CreatingInvestors :"Créer des investisseurs fiers.",
    CreatingInvestorsDescription:"Vous pouvez sentir la fierté d'être un investisseur Groww dans leurs mots.",
    InvestorDescription:"Groww.in a été la plate-forme où j'ai intégré MF pour la première fois et je dois dire que même pour un débutant comme moi, cela a rendu les choses plus faciles à explorer et à investir.",
    InvestorName :" Ankit Puri",
    InvestorTitle :" Spécialiste produit, Google",
}