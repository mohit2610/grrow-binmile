import LoginScreens from "./Language/LanguageHeader/LangHeaderEnglish";
import Articles from "./Language/LanguageArticle/LanguageArticlesEnglish";
import LanguageHomeScreen from "./Language/LanguageHomeScreen/LanguageHomeScreenEnglish";
import LanguageInvestors from "./Language/LanguageInvestors/LanguageInvestorsEnglish";

export default {
    ...Articles,
    ...LoginScreens,
    ...LanguageHomeScreen,
    ...LanguageInvestors,
}