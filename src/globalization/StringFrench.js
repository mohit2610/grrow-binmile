import LangHeaderFrench from "./Language/LanguageHeader/LangHeaderFrench";
import LanguageArticlesFrench from "./Language/LanguageArticle/LanguageArticlesFrench"
import LanguageHomeScreenFrench from "./Language/LanguageHomeScreen/LanguageHomeScreenFrench"
import LanguageInvestorsFrench from "./Language/LanguageInvestors/LanguageInvestorsFrench";

export default {
    ...LangHeaderFrench,
    ...LanguageArticlesFrench,
    ...LanguageHomeScreenFrench ,
    ...LanguageInvestorsFrench
}