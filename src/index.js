import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import AppRoutes from "./AppRoutes";
import * as Sentry from "@sentry/react";
import { BrowserTracing } from "@sentry/tracing";
import { Provider } from "react-redux";
import { LocalizationProvider } from "./localization/Localization";
import { AuthProvider } from "./contexts/AuthContext";
import { store } from "./Redux-Tolkit/Store"

Sentry.init({
  dsn: "https://5e038c07a130488b9e1131b9b4509331@o1179999.ingest.sentry.io/6292448",
  integrations: [new BrowserTracing()],
  tracesSampleRate: 1.0,
});

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <LocalizationProvider>
        <AuthProvider>
          <AppRoutes />
        </AuthProvider>
      </LocalizationProvider>
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);