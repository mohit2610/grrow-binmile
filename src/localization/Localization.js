import React, { createContext, useMemo, useState, } from 'react';
import english from '../globalization/StringEnglish';
import french from '../globalization/StringFrench';


export const LANGUAGE_ENGLISH = 'en';
export const LANGUAGE_FRENCH = 'fr';
const DEFAULT_LANGUAGE = LANGUAGE_ENGLISH;

export const LocalizationContext = createContext({});

export function LocalizationProvider({ children }) {
    const [appLanguage, setAppLanguage] = useState(DEFAULT_LANGUAGE);
    const translations = {
        [LANGUAGE_ENGLISH]: english,
        [LANGUAGE_FRENCH]: french,
    };
    const mProps = useMemo(() => ({
        appLanguage,
        setAppLanguage,
        translations: translations[appLanguage],
    }), [appLanguage]);

    return (
        <LocalizationContext.Provider value={mProps}>
            {children}
        </LocalizationContext.Provider>
    );
}