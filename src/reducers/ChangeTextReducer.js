let initialState = {count: 'mohit'}

function ChangeTextReducer(state = initialState, action) {
  if(action.type === 'CHANGE_TEXT') {
    return {
      count: action.payload
    };
  }

  return state;
}

export default ChangeTextReducer;
