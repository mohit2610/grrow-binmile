import ChangeTextReducer from "../reducers/ChangeTextReducer";
import { combineReducers } from 'redux'

export default combineReducers({
    ChangeTextReducer
})